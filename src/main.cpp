#include <iostream>
#include <cstdio>
#include <fstream>
#include <vector>

using namespace std;

vector<string> *stop_word_loader(const string& location);

int main(int argc, char *argv[]){
    string location = argc == 2 ? argv[1] : "../dat/Lab05_stop-word-list.txt";
    vector<string> *stop_Words = stop_word_loader(location);

    cout << "Insert your message that you believe is spam: " << endl;
    string message;

    //TODO popraviti unos teksta, ili koristiti datoteke ili u Qt-u napisati
    string line;
    while(getline(cin, line)){
        message += line;
    }


    cout << endl << message << endl << endl;

    delete stop_Words;

    return 0;
}

vector<string> *stop_word_loader(const string& location){
    auto *words = new vector<string>;
    string word;

    ifstream stopwords_file(location);
    if(stopwords_file.is_open()){
        while(getline(stopwords_file, word)){
            words->push_back(word);
        }
        stopwords_file.close();
    } else{
        cerr << "Stop word list not found!" << endl << "Check your directory" << endl;
    }

    return words;
}